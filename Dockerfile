FROM node:latest
ARGS NODE_ENV
COPY src/ app/
COPY *.json /app/
COPY *.js /app/
WORKDIR /app
RUN npm install pm2@latest -g && npm install
ENTRYPOINT ["pm2","start","backend.json"]



