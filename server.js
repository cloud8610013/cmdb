
const express = require('express');
const users = require('./src/routes/users')
const app = express();
app.use(express.json());
app.use('/api/v1/users', users)
const dotenv = require('dotenv').config()

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server listening in ${process.env.NODE_ENV} mode on port ${port}`));

